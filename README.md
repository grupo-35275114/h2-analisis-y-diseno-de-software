# Grupo 3  

Este es el repositorio del *Grupo 3*, cuyos integrantes son:

* Pedro Arce - 202056597-k
* Fabian Miranda - 202030515-3 
* Fernando Carrasco - 202030542-0

## Wiki

Puede acceder a la wiki mediante el siguiente [enlace](https://gitlab.com/grupo-35275114/h2-analisis-y-diseno-de-software/-/wikis/home)

## Videos

* [Video presentación cliente](https://youtu.be/NN5byxZM8Fc)

## Aspectos técnicos relevantes

